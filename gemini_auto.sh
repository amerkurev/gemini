NAME=$1

VCF="$NAME.vcf"
VCF_NORM="$NAME.norm.vcf"
VCF_ANNOT="$NAME.annot.vcf"
DB="$NAME.db"
TSV="$NAME.tsv"
FASTA=/root/fasta/human_g1k_v37.fasta

echo
echo VCF........$VCF
echo VCF_NORM...$VCF_NORM
echo VCF_ANNOT..$VCF_ANNOT
echo DB.........$DB
echo TSV........$TSV
echo FASTA......$FASTA
echo

echo 'Step 1. split, left-align, and trim variants'
/root/vt/vt decompose -s $VCF | /root/vt/vt normalize -r $FASTA - > $VCF_NORM

echo 'Step 2. Annotate with VEP'
/root/ensembl-vep/vep -i $VCF_NORM \
    --offline \
    --fasta $FASTA \
    --merge \
    --cache \
    --sift b \
    --polyphen b \
    --symbol \
    --numbers \
    --biotype \
    --total_length \
    -o $VCF_ANNOT \
    --vcf \
    --fields Consequence,Codons,Amino_acids,Gene,SYMBOL,Feature,EXON,PolyPhen,SIFT,Protein_position,BIOTYPE

echo 'Step 3. Load to the Gemini VEP-annotated VCF'
gemini load --cores 2 -v $VCF_ANNOT -t VEP $DB

echo 'Step 4. Export to TSV'
gemini query --header -q "select chrom, start, end, gene, rs_ids, ref, alt, type, in_omim, clinvar_sig, clinvar_causal_allele, polyphen_pred, sift_pred, in_dbsnp, clinvar_gene_phenotype, aaf_esp_all, aaf_1kg_all, aaf_exac_all, aaf_gnomad_all, exon, is_exonic, is_coding, impact, impact_severity, transcript from variants where aaf_esp_all <= 0.01 and aaf_1kg_all <= 0.01 and aaf_exac_all <= 0.01 and aaf_gnomad_all <= 0.01" $DB > $TSV 2>&1
